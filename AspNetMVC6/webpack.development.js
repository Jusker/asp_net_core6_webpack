﻿const { merge } = require('webpack-merge');
const  common  = require('./webpack.config.js');
const path = require('path');
const ExtractCssPlugin = require('mini-css-extract-plugin');
const webpack = require('webpack')

const webpackDevServerPort = 8088;
const proxyTarget = "http://localhost:7066";
const frontServer = "http://localhost:" + webpackDevServerPort;

module.exports = merge(common, {
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: {
        allowedHosts: 'all',
        compress: true,
        https: false,
        //Добавляется автоматически
        //hot: true,


        //historyApiFallback: true,
        static: {
            directory: path.join(__dirname, 'wwwroot/dist'),
            serveIndex: true,
        },
        //proxy: {
        //    "/dist": {
        //        target: proxyTarget,
        //    },
        //},
        //    //bypass: function (req, res, proxyOptions) {
        //    //    //if (req.headers.accept.indexOf('html') !== -1) {
        //    //    console.log('Skipping proxy for browser request.', req, res, proxyOptions);
        //    //    return '/index.html';
        //    //    //}
        //    //},
        //    //'wwwroot/dist': proxyTarget
        //},
        //proxy: {
        //    '*': {
        //        target: proxyTarget
        //    }
        
        port: webpackDevServerPort,

        //onListening: function (devServer) {
        //    if (!devServer) {
        //        throw new Error('webpack-dev-server is not defined');
        //    }

        //    const port = devServer.server.address().port;
        //    console.log('Listening on port:', port);
        //},
        //devMiddleware: {
        //    index: true,
        //    publicPath: path.join(__dirname, 'wwwroot/dist'),
        //    writeToDisk: true
        //},
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ]
});