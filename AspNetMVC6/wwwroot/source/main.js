﻿import React from 'react';
import ReactDOM from 'react-dom';



export default class TestComponent extends React.Component {

    render() {
        return <div>Hello, { this.props.name }</div>;
    }

}


ReactDOM.render(
    <TestComponent name="Артём" />,
    document.getElementById('mainDiv')
);