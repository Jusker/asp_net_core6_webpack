﻿// webpack.config.js
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');


const config = {

        mode: 'development',
        //Список файлов для начала сборки бандлов
        entry: {
            //main: path.resolve(__dirname, './wwwroot/source/main.js'),
            main: path.resolve(__dirname, 'wwwroot/source/testSource.js'),

        },
        
        //plugins: [
        //    new HtmlWebpackPlugin({
        //        title: 'Development',
        //        template: './wwwwroot/template.html'
        //    }),
        //],
        //Выходные файлы - бандлы
        output: {
            //путь до бандлов
            path: path.resolve(__dirname, 'wwwroot/dist'),
            //формат названия файла
            filename: '[name].bundle.js',
            clean: true
        },
        module: {
            rules: [   //загрузчик для jsx
                {
                    test: /\.(js|jsx)?$/, // определяем тип файлов
                    exclude: /(node_modules)/,  // исключаем из обработки папку node_modules
                    loader: "babel-loader",   // определяем загрузчик
                    options: {
                        presets: ["@babel/preset-env", "@babel/preset-react"]    // используемые плагины
                    }
                },
                {
                    test: /\.html$/,
                    use: [{
                        loader: "html-loader",
                        options: {
                            minimize: true
                        }
                    }]
                }
            ],
        },
        //module: {
        //    rules: [
        //        {
        //            test: /\.css$/i,
        //            use: ['style-loader', 'css-loader'],
        //        },
        //    ],
        //},

};


module.exports = config;