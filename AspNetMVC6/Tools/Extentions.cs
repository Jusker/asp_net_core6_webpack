﻿using System.Management.Automation;

namespace AspNetMVC6.Tools
{
    public static class Extentions
    {
        public static WebApplication UseWebpackServer(this WebApplication hostBuilder) {

            //Task task = new Task(async () => await RunScript("npm run build-hot"), TaskCreationOptions.AttachedToParent);
            Console.WriteLine("Test1");

            //RunScriptSync("npm run build-hot");

            //Определяем текущий порт и разворачиваем сервер на следующем
            int port = new Uri(hostBuilder.Configuration["urls"]).Port + 1;
            Task.Run(() => RunScript($"npm run build-hot -- --port {port}"));

            //await RunScript("npm run build-hot");


            hostBuilder.UseEndpoints(endpoints => {
                endpoints.MapGet("/dist/{bundle_name:regex(.+\\.js)}", context =>
                {
                    var name = context.Request.RouteValues["bundle_name"];
                    context.Response.Redirect($"http://localhost:{port}/{name}");
                    //await context.Response.WriteAsync("Hello World!");
                    return Task.FromResult(0);
                });
            });

            return hostBuilder;
        }

        /// <summary>
        /// Runs a PowerShell script with parameters and prints the resulting pipeline objects to the console output. 
        /// </summary>
        /// <param name="scriptContents">The script file contents.</param>
        /// <param name="scriptParameters">A dictionary of parameter names and parameter values.</param>
        public static async Task RunScript(string scriptContents)
        {
            // create a new hosted PowerShell instance using the default runspace.
            // wrap in a using statement to ensure resources are cleaned up.

            using (PowerShell ps = PowerShell.Create())
            {
                
                // specify the script code to run.
                ps.AddScript(scriptContents);
                // execute the script and await the result.
                var pipelineObjects = await ps.InvokeAsync().ConfigureAwait(false);

            }
        }
    }
}
